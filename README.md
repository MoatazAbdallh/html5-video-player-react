# HTML5-Interactive-Video-Player-React

#### This task for creating Interactive HTMl5 Video Player using the following stacks:
- ReactJS 16.4
- React-Semantic-UI
- React-Semantic-Slide-Control ( I have used this control
as i have tried to implement my own progress by equation
`Math.floor((currentTime/Duration)*100)` but this may not be seconds precise
so i have used this control with steps and callbacks whenever manual seeking,
But i think it has little bit performance issue as i figure out from
performance profiler simulating cup throttling x4, in future i can go through
refining the above equation

![Performance Profiling](https://i.imgur.com/CAvMxVg.jpg)

- Webpack ( Enabling CSS Modules, Chunk Vendor node_modules,sass style loader  )
- ES6
- Json API (Simple Node Server, deployed over aws Lambda
& triggered through api gateway)
 to store information about hotspots to simulate sending an actual request with axios.
[Json Schema](https://s3-us-west-1.amazonaws.com/interactive-player-assets/schema.json)

 ![Backend design](https://i.imgur.com/fnRncDh.png)

- Deployed over S3 as a static website with enabled CloudFront
( To be done in future adding support for jenkins CI/CD with
 following flow for deployment through adding webhook on released tags
 on master branch then jenkins build task will be fired and run yarn build
 with aws cli integration & IAM role will sync new build to production s3 bucket :).

- for now i have added two scripts in package.json which responsible for deploy to s3 and invalidate cloudfront distribution

### Project Structure
- Components (Reusable Units)
    ---> Player (Stateless Component)
    ---> PlayerControls (Stateless Component responsible to fire player controls handlers)
    ---> SeekingBarWithHotspots (State Component which responsible for seeking & handling hotspots hover & click handlers)
    ---> PlayerWithEvents (State Component source of truth of implementing different handlers)
- Containers
    ---> VideoPlayerBuilder (State Component responsible to retrieve data @ ComponentDidMount)
- HOC (The upper hand for common & events handler logic)
    ---> axiosWithErrorHandler (Responsible to intercept request and handle error messages)
    ---> PageLayout

### Website
Only Access this website & all things will work together :)
[Deployed Website](https://d2zbta88puk4vy.cloudfront.net/)

### Future Work
- Using Jest with Enzyme to implement test suites for variations in components contract.
- Using testcafe or nightwatch to have e2e test suites