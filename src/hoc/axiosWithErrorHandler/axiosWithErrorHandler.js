import React, {Component} from 'react';
import ModalWithActions from '../../components/ModalWithActions/ModalWithActions';

const axiosWithErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {


        componentWillMount() {
            this.reqInterceptor = axios.interceptors.request.use(req => {
                this.setState({error: null});
                return req;
            });
            this.resInterceptor = axios.interceptors.response.use(res => res, error => {
                this.setState({error: error});
            });
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor);
        }

        handleClose = () => {
            this.setState({errorCode: null});
        };

        state = {
            errorCode: null,
            errorMessage: null,
            modalActions: [{
                label: "Return",
                handler: this.handleClose
            }]
        };

        render() {
            return (
                <React.Fragment>
                    <ModalWithActions errorCode={this.state.errorCode}
                                      errorMessage={this.state.errorMessage}
                                      modalActions={this.state.modalActions}
                                      size='small'/>
                    <WrappedComponent {...this.props} />
                </React.Fragment>
            );
        }
    }
};

export default axiosWithErrorHandler;