import React, {Component} from 'react';
import HeaderImage from '../../components/Header/HeaderImage'
import Footer from '../../components/Footer/Footer'
import pageLayoutClasses from './PageLayout.scss';
import {Segment, Divider, Container} from 'semantic-ui-react'

class PageLayout extends Component {

    render() {
        return (
            <Container className={pageLayoutClasses.containerWrapper}>
                <HeaderImage/>
                <Segment textAlign='center' size='big' color='green' raised>
                    {this.props.children}
                </Segment>
                <Divider clearing/>
                <Footer/>
            </Container>
        )
    }
}

export default PageLayout;