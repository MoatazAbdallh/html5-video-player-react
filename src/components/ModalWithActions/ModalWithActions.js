import React, {Component} from 'react';
import {Modal, Button} from 'semantic-ui-react'


class ModalWithActions extends Component {

    state = {
        errorCode: null,
        errorMessage: null,
        modalActions: []
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        //this Object will be merged on prevState
        return {...nextProps}
    }

    render() {
        let modalActions = [];
        if (this.state.modalActions) {
            modalActions = this.state.modalActions.map((action) => {
                return <Button key={action.label} onClick={action.handler}>{action.label}</Button>
            })
        }
        return (
            <Modal dimmer='blurring' size={this.state.size}
                   open={this.state.errorCode !== null}>
                <Modal.Content>
                    <p>{this.state.errorMessage ? this.state.errorMessage : null}
                    </p>
                </Modal.Content>
                <Modal.Actions>
                    {modalActions}
                </Modal.Actions>
            </Modal>
        );
    }
}

export default ModalWithActions;
