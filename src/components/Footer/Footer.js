import React from 'react';
import FooterClasses from './Footer.css';

const Footer = () => {
    return (
        <div className={FooterClasses.centerAligned}>
            Copyright © 2018 Moataz Hammous
        </div>
    )
};
export default Footer;