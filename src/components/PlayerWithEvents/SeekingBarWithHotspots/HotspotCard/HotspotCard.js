import React, {Component} from 'react';
import {Icon, Popup, Card} from 'semantic-ui-react'
import LazyImage from '../../../LazyImage/LazyImage';
import {Link, withRouter} from 'react-router-dom';

class HotspotCard extends Component {

    componentWillReceiveProps(newProps) {
        if (newProps.location.search !== this.props.location.search) {
            const query = new URLSearchParams(newProps.location.search);
            for (let param of query.entries()) {
                if (param.length > 1) {
                    if (param[0] === 'entryTime' && param[1] !== this.queryEntryPoint) {
                        this.props.hotspotClickHandler(param[1])
                    }
                }
            }
        }
    }

    render() {
        const hotSpotIcon =
            <span style={this.props.hotspotPositionStyle}>
        <Link to={{
            pathname: 'hotspots',
            search: `?entryTime=${this.props.hotspot.entryTime}`
        }}>
            <Icon size='small' color='blue'
                  key={this.props.hotspot.id} link
                  name='star'/>
        </Link>
            </span>;
        return (
            <Popup key={this.props.hotspot.id} hoverable position='top center' verticalOffset={20}
                   trigger={hotSpotIcon}>
                <Card style={{width: '15rem'}} color='violet' key={this.props.hotspot.id}>
                    <LazyImage src={this.props.hotspot.imageUrl}/>
                    <Card.Content>
                        <Card.Description>{this.props.hotspot.description}</Card.Description>
                    </Card.Content>
                </Card>
            </Popup>
        )
    }
};

export default withRouter(HotspotCard);