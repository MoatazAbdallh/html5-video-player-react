import React, {PureComponent} from 'react';
import SeekingBarWithHotspotsClasses from './SeekingBarWithHotspots.scss';
import {Slider} from 'react-semantic-ui-range'
import {withRouter} from 'react-router-dom';
import HotspotCard from './HotspotCard/HotspotCard';
import PropTypes from "prop-types";

class SeekingBarWithHotspots extends PureComponent {

    state = {
        currentTime: 0,
        duration: 0,
        hotspots: [],
        seekToHandler: null
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        //this Object will be merged on prevState
        return {
            currentTime: nextProps.currentTime,
            duration: nextProps.duration,
            hotspots: nextProps.hotspots ? [...nextProps.hotspots] : [],
            seekToHandler: nextProps.seekToHandler
        }
    }

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search);
        for (let param of query.entries()) {
            if (param.length > 1) {
                if (param[0] === 'entryTime') {
                    this.hotspotClickHandler(param[1])
                }
            }
        }
    }

    hotspotClickHandler = (hotSpotEntryTime) => {
        this.state.seekToHandler(hotSpotEntryTime);
    };

    render() {
        const sliderSettings = {
            start: this.state.currentTime,
            min: 0,
            max: this.state.duration,
            step: 1,
            onChange: (value) => {
                if (value !== this.state.currentTime) {
                    this.state.seekToHandler(value)
                }
            }
        };

        //Editing hotspots to add new properties like visible & marginLeft position
        const hotspots = this.state.hotspots.map(hotspot => {
            const marginLeft = (hotspot.entryTime / this.state.duration) * 100;
            const hotspotPositionStyle = {
                left: `${marginLeft}%`,
                position: 'absolute'
            };

            return <HotspotCard hotspotClickHandler={this.hotspotClickHandler} key={hotspot.id} hotspot={hotspot}
                                hotspotPositionStyle={hotspotPositionStyle}/>
        });

        return (
            <div className={SeekingBarWithHotspotsClasses.seekingBarHotspotsWrapper}>
                <div className={SeekingBarWithHotspotsClasses.seekingBarWrapper}>
                    <Slider
                        color="grey" value={this.state.currentTime} inverted={false}
                        settings={sliderSettings}/>
                </div>
                <div className={SeekingBarWithHotspotsClasses.hotspotsWrapper}>
                    {hotspots}
                </div>
            </div>
        )
    }
}

export default withRouter(SeekingBarWithHotspots);

SeekingBarWithHotspots.propTypes = {
    currentTime: PropTypes.number.isRequired,
    duration: PropTypes.number.isRequired,
    seekToHandler: PropTypes.func.isRequired,
    hotspots: PropTypes.array
};