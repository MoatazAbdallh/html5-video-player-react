import Player from './Player/Player';
import PlayerControls from './PlayerControls/PlayerControls'
import ModalWithActions from '../ModalWithActions/ModalWithActions';
import React, {Component} from 'react';
import {Dimmer, Loader} from 'semantic-ui-react';
import PlayerWithEventsClasses from './PlayerWithEvents.scss'
import SeekingBarWithHotspots from './SeekingBarWithHotspots/SeekingBarWithHotspots';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';

import moment from 'moment';


class PlayerWithEvents extends Component {

    state = {
        playerError: {
            errorCode: null,
            errorMessage: null,
            modalActions: []
        },
        playerTimes: {
            currentTimeFormat: '00:00:00',
            durationFormat: '00:00:00',
            currentTime: 0,
            duration: 0
        },
        activateLoader: true,
        videoSrc: '',
        hotspots: [],
        containerWidth: 0
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        //this Object will be merged on prevState
        return {
            ...nextProps
        }
    }


    registerVideoEventsHandler() {
        //Binding VideoElement Events through ref callback
        this.videoElement.addEventListener('loadedmetadata', this.loadedMetadataHandler);
        this.videoElement.addEventListener('timeupdate', this.timeUpdateHandler);
        this.videoElement.addEventListener('error', this.errorHandler);
        // this.videoElement.addEventListener('seeking', this.state.seekingHandler);
        // this.videoElement.addEventListener('seeked', this.state.seekedHandler);
        this.videoElement.addEventListener('play', this.playHandler);
        this.videoElement.addEventListener('pause', this.pauseHandler);
        this.videoElement.addEventListener('canplay', this.canPlayHandler);
        this.videoElement.addEventListener('waiting', this.waitingHandler);
    }

    loadedMetadataHandler = () => {
        const duration = moment.duration(this.videoElement.duration, 'seconds');
        //As setState executed asynchronously
        this.setState((prevState, props) => {
            const playerTimes = {...prevState.playerTimes}; //immutable
            playerTimes.durationFormat = duration.hours() + ":" + duration.minutes() + ":" + duration.seconds();
            playerTimes.duration = this.videoElement.duration;
            return {playerTimes}
        })
    };
    timeUpdateHandler = () => {
        const currentTime = moment.duration(this.videoElement.currentTime, 'seconds');
        //As setState executed asynchronously
        this.setState((prevState, props) => {
            const playerTimes = {...prevState.playerTimes}; //immutable
            playerTimes.currentTimeFormat = currentTime.hours() + ":" + currentTime.minutes() + ":" + currentTime.seconds();
            playerTimes.currentTime = this.videoElement.currentTime;
            return {playerTimes}
        })
    };
    playHandler = () => {
        this.videoElement.play();
    };
    pauseHandler = () => {
        this.videoElement.pause();
    };
    forwardHandler = () => {
        this.videoElement.currentTime += 5;
    };
    rewindHandler = () => {
        this.videoElement.currentTime -= 5;
    };
    waitingHandler = () => {
        this.setState({
            activateLoader: true
        })
    };

    canPlayHandler = () => {
        this.setState({
            activateLoader: false
        })
    };
    errorHandler = () => {
        this.setState({
            playerError: {
                errorCode: this.videoElement.error.code,
                errorMessage: this.videoElement.error.message,
                modalActions: [{
                    label: "Return",
                    handler: this.handleModalClose
                }]
            }
        })
    };
    handleModalClose = () => {
        this.setState({playerError: {errorCode: null}});
    };
    seekToHandler = (seekTime) => {
        //In case videoElement not rendered yet
        if (!this.videoElement) {
            this.props.history.push({pathname: '/'});
            return
        }
        this.videoElement.currentTime = seekTime;
    };

    render() {
        const videoSrcIsEmpty = !this.state.videoSrc || /^\s*$/.test(this.state.videoSrc);
        if (this.videoElement && !this.isVideoEventsRegistered) {
            this.isVideoEventsRegistered = true;
            this.registerVideoEventsHandler();
        }
        const videoPlayer = videoSrcIsEmpty ? null : <Player videoRef={el => this.videoElement = el}
                                                             videoSrc={this.state.videoSrc}
                                                             containerWidth={this.state.containerWidth}
                                                             registerVideoEvents={this.registerVideoEventsHandler}/>;
        return (
            <React.Fragment>

                <ModalWithActions errorCode={this.state.playerError.errorCode}
                                  errorMessage={this.state.playerError.errorMessage}
                                  modalActions={this.state.playerError.modalActions}
                                  size='mini'/>
                <div className={PlayerWithEventsClasses.playerWrapper}>
                    <Dimmer active={this.state.activateLoader} inverted inline='centered'>
                        <Loader inverted>Buffering</Loader>
                    </Dimmer>
                    {videoPlayer}
                    <SeekingBarWithHotspots currentTime={this.state.playerTimes.currentTime}
                                            duration={this.state.playerTimes.duration}
                                            hotspots={this.state.hotspots}
                                            seekToHandler={this.seekToHandler}/>
                </div>


                <PlayerControls currentTime={this.state.playerTimes.currentTimeFormat}
                                duration={this.state.playerTimes.durationFormat}
                                playHandler={this.playHandler}
                                pauseHandler={this.pauseHandler}
                                forwardHandler={this.forwardHandler}
                                rewindHandler={this.rewindHandler}/>

            </React.Fragment>
        )
    }

}

export default withRouter(PlayerWithEvents);

PlayerWithEvents.propTypes = {
    videoSrc: PropTypes.string.isRequired,
    hotspots: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        description: PropTypes.string,
        imageUrl: PropTypes.string.isRequired,
        entryTime: PropTypes.number.isRequired
    })).isRequired,
    containerWidth: PropTypes.number.isRequired
};