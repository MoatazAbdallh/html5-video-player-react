import React from 'react';
import {Segment, Button} from 'semantic-ui-react';
import PlayerControlsClasses from './PlayerControls.scss';
import PropTypes from "prop-types";

const PlayerControls = (props) => {

    return (
        <Segment size='mini' className={PlayerControlsClasses.controlsDurationWrapper} textAlign='center' color='blue'
                 raised>
            <div className={PlayerControlsClasses.controlsWrapper}>
                <Button.Group>
                    <Button icon='angle double left'
                            onClick={props.rewindHandler}/>
                    <Button icon='play' onClick={props.playHandler}/>
                    <Button icon='pause' onClick={props.pauseHandler}/>
                    <Button icon='angle double right'
                            onClick={props.forwardHandler}/>
                </Button.Group>
            </div>
            <div className={PlayerControlsClasses.durationWrapper}>
                {props.currentTime}
                / {props.duration}
            </div>
        </Segment>
    )
};

export default PlayerControls;

PlayerControls.propTypes = {
    rewindHandler: PropTypes.func.isRequired,
    playHandler: PropTypes.func.isRequired,
    pauseHandler: PropTypes.func.isRequired,
    forwardHandler: PropTypes.func.isRequired,
    currentTime: PropTypes.string.isRequired,
    duration: PropTypes.string.isRequired
};