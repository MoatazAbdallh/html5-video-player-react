import React from 'react';
import PropTypes from "prop-types";

const Player = (props) => {
    const height = props.containerWidth * 850 / 1920;
    return (
        <video ref={props.videoRef}
               src={props.videoSrc}
               width={props.containerWidth}
               height={height}
               autoPlay="autoplay"
               id="video-player"/>
    )
};

export default Player;

Player.propTypes = {
    videoSrc: PropTypes.string.isRequired,
    containerWidth: PropTypes.number.isRequired,
    videoRef: PropTypes.func.isRequired
};