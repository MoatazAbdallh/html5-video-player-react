import {Image as ReactImage, Loader, Label} from 'semantic-ui-react'
import React, {Component} from 'react';

class LazyImage extends Component {

    constructor(props) {
        super(props);
        this.imageElement = React.createRef();
    }

    state = {
        showImage: false,
    };

    componentDidMount() {
        this._mounted = true;
        const imageElement = new Image();
        imageElement.src = this.props.src;
        imageElement.addEventListener('load', this.imageLoadedHandler);
        imageElement.addEventListener('error', this.imageErrorHandler)
    }

    componentWillUnmount() {
        this._mounted = false
    }

    imageLoadedHandler = () => {
        if (this._mounted) {
            this.setState({
                showImage: true
            })
        }
    };

    imageErrorHandler = () => {
        if (this._mounted) {
            this.setState({
                showErrorLabel: true
            })
        }
    };

    render() {
        return (
            <React.Fragment>
                <Loader active={!this.state.showImage} inline="centered"/>
                <ReactImage {...this.props} >
                    {this.state.showErrorLabel ? <Label content='Image not found!' icon='warning'/> : null}
                </ReactImage>
            </React.Fragment>
        )
    }
}

export default LazyImage;