import React from 'react';
import {Header, Image} from 'semantic-ui-react'
import iPlayerLogo from '../../assets/player-logo.svg';

const HeaderImage = () => {
    return (
        <Header as='h2'>
            <Image src={iPlayerLogo}/>
            <Header.Content>
                IPlayer
                <Header.Subheader>Interactive HTML5 Video Player</Header.Subheader>
            </Header.Content>
        </Header>
    )
};
export default HeaderImage;