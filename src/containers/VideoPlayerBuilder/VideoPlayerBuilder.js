import React, {Component} from 'react';
import axiosWithErrorHandler from '../../hoc/axiosWithErrorHandler/axiosWithErrorHandler';
import axios from '../../axios-instance';
import PlayerWithEvents from '../../components/PlayerWithEvents/PlayerWithEvents';
import ContainerDimensions from 'react-container-dimensions';

class VideoPlayerBuilder extends Component {

    //TODO (Finished)
    // 1- Request video hotspots through axios & update state during componentDidMount lifeCycle
    // 2- render video component with it's props
    // 3- render seeking component with it's props
    // 4- render player controls component (FastForward, Reward, Play/Pause)
    // 5- Adding Hotspots handlers

    state = {
        currentVideoSrc: '',
        hotspots: []
    };

    componentDidMount() {
        //Requesting the 1st video, but in future we can have side menu with all videos and request data per user selection
        axios.get('/videos/1')
            .then(response => {
                this.setState({
                    currentVideoSrc: response.data.url,
                    hotspots: response.data.hotspots
                });
            })
    }

    render() {
        return (
            <div>
                <ContainerDimensions>
                    {
                        ({width}) =>
                            <PlayerWithEvents videoSrc={this.state.currentVideoSrc}
                                              hotspots={this.state.hotspots}
                                              containerWidth={width}/>
                    }
                </ContainerDimensions>
            </div>
        )
    }
}

export default axiosWithErrorHandler(VideoPlayerBuilder, axios);