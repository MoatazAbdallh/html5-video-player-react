import React, {Component} from 'react';
import 'semantic-ui-css/semantic.min.css';
import {BrowserRouter, Route} from 'react-router-dom';

import PageLayout from './hoc/PageLayout/PageLayout';
import VideoPlayerBuilder from './containers/VideoPlayerBuilder/VideoPlayerBuilder';

class App extends Component {

    render() {

        return (
            <BrowserRouter>
                <div>
                    <PageLayout>
                        <Route path='/' component={VideoPlayerBuilder}/>
                    </PageLayout>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
