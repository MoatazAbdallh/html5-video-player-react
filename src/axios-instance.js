import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://2krnmzvll0.execute-api.us-east-1.amazonaws.com/latest/'
});

export default instance;